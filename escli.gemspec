# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'canzea/version'

Gem::Specification.new do |spec|
  spec.name          = "escli"
  spec.version       = Canzea::VERSION
  spec.authors       = ["Canzea Technologies"]
  spec.email         = ["ikethecoder@canzea.com"]

  spec.summary       = %q{Canzea command line interface for orchestrating builds.}
  spec.description   = %q{Orchestrate the building of images based on defined ecosystem blueprints.}
  spec.homepage      = "http://www.canzea.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "bin"

  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"

  spec.add_runtime_dependency "net-ssh", "~> 4.1.0"
  spec.add_runtime_dependency "net-sftp", "~> 2.1"
  spec.add_runtime_dependency "mustache", "~> 1.0"
  spec.add_runtime_dependency "cri", "~> 2.4.1"
  spec.add_runtime_dependency "git", "~> 1.3"
end

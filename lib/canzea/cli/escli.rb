require 'cri'
require 'fileutils'
require 'pathname'
require 'git'
require 'io/console'
require "canzea/config"
require "canzea/version"
require "canzea/environment"
require "canzea/helper-run-class"
require "canzea/plan-step-class"
require "canzea/core/trace-component"
require "canzea/core/ssh-base-cmd-class"
require "canzea/core/template-runner"
require "canzea/commands/login"
require "canzea/commands/get-catalog"
require "canzea/commands/ecosystem/ecosystem"
require "canzea/commands/ecosystem/resources"


=begin

escli login
escli logout
escli ecosystem list
escli ecosystem set 23432894382932
escli resource plus test.yaml prod-1
escli resource apply f67c70b0-2706-48b5-ac4b-c76eeb72effe


escli workspace list


escli ecosystem create/destroy
escli workspace build
escli block install
escli block configure
escli project add

escli resource get block gocd/docker

escli resource list
escli resource check
escli resource plus
escli resource minus

escli job submit

escli ecosystem set 23432894382932
escli ecosystem get 23432894382932

All “helpers” have a GET and LIST

=end

module Canzea

  root_cmd = Cri::Command.define do

      name        'escli'
      usage       'escli [options]'
      summary     'escli cli'
      description 'Canzea Ecosystem command line interface.'

      flag   :h,  :help,  'show help for this command' do |value, cmd|
        puts cmd.help
        exit 0
      end

      flag :v, :version, 'Version' do |value, cmd|
        puts "ES CLI: v#{Canzea::VERSION}"
      end

      flag nil, :reset, 'Refresh catalog to latest'
      flag nil, :fv, 'Show version and the catalog version'

      option nil, :config, 'Config', argument: :required
      option nil, :catalogTag, 'Specific tag of the catalog', argument: :required

      run do |opts, args, cmd|

        test = opts.fetch(:test, false)

        if opts[:config]
            file = File.read(opts[:config])
            Canzea::configure JSON.parse(file)
        end

        extraConfig = Canzea::config[:config_location] + "/config.json"
        if File.exists?(extraConfig)
            if (opts[:verbose])
                puts "-- Reading #{extraConfig}"
            end
            file = File.read(extraConfig)
            Canzea::configure JSON.parse(file)
        end

        if File.exists?(Canzea::config[:catalog_location]) == false
            GetCatalog.new.do(opts.fetch(:catalogTag, nil));
        end

        if (opts[:reset])
            GetCatalog.new.do(opts.fetch(:catalogTag, nil));
        end

        GetCatalog.new.state

        if (opts[:fv])
            puts "ES CLI: v#{Canzea::VERSION}"

            puts "Catalog: #{ENV['CATALOG_BRANCH']} ( #{ENV['CATALOG_COMMIT']} )"
        end


        args.each do |arg|
          puts "Publishing #{arg}…"
        end
      end

  end

  root_cmd.define_command do
    name        'logout'
    usage       'logout [options]'
    summary     'Clears credentials'
    description 'This command clears credentials.'

    run do |opts, args, cmd|
        Login.new.logout
    end
  end


  root_cmd.define_command('login') do
    name        'login'
    usage       'login'
    summary     'Authenticates and stores token.'

    run do |opts, args, cmd|

        print "Enter your username: "
        username = STDIN.gets.chomp

        print "Enter your password: "
        password = STDIN.noecho(&:gets).chomp
        puts ""
        Login.new.do username, password
    end
  end

  escmd = root_cmd.define_command('ecosystem') do
    name        'ecosystem'
    usage       'ecosystem [options]'
    summary     'Manage an ecosystem (create, destroy, list, set)'

    run do |opts, args, cmd|
    end
  end

  escmd.define_command('set') do
    usage       'set [id]'
    summary     'Sets the ecosystem ID.'

    run do |opts, args, cmd|
        Ecosystem.new.set args[0]
    end
  end

  escmd.define_command('list') do
    usage       'list'
    summary     'List all ecosystems for account'
    run do |opts, args, cmd|
        Ecosystem.new.list
    end
  end

  escmd.define_command('create') do
    usage       'create [options]'
    summary     'Create an ecosystem'
  end

  escmd.define_command('destroy') do
    usage       'destroy [options]'
    summary     'Destroy an ecosystem'
  end

  escmd.define_command('info') do
    usage       'info [options]'
    summary     'Info about currently set ecosystem'
  end

  root_cmd.define_command('resource') do
    name        'resource'
    usage       'resource plus [manifest File] [segment]'
    summary     'Apply resources to the environment'

    run do |opts, args, cmd|
        if args[0] == 'plus'
            Resources.new.plus args[1], args[2]
        end
        if args[0] == 'apply'
            Resources.new.apply args[1]
        end
    end
  end

  root_cmd.run(ARGV)

end

require 'json'
require 'logger'
require "base64"

require "canzea/core/prepare-environment"
require "canzea/core/trace-runner"

class HelperRun
    def initialize (_raw)
        @basePath = "#{Pathname.new(Canzea::config[:catalog_location]).realpath}"
        @log = Logger.new(Canzea::config[:logging_root] + '/plans.log')
        @raw = _raw
    end

    def runPlan (plans)
        plans.each { | plan |
            puts "Running #{plan[:action]}"
            self.run plan[:solution], plan[:action], JSON.generate(plan[:parameters])
        }
    end

    def enrich (params)
        # If there is context information, then merge them into the parameters before calling the helper
        if (ENV.has_key?('WORK_DIR') and File.exists?ENV['WORK_DIR'] + "/context.json")
            context = JSON.parse(File.read(ENV['WORK_DIR'] + "/context.json"))
            result = JSON.parse(params).merge(context)
            return JSON.generate(result)
        end
        return params
    end

    def run (solution, action, parameters, status = false)

        type = "ruby"

        parameters = self.enrich(parameters)

        envPush = PrepareEnvironment.new @raw

        begin
            root = "#{@basePath}/helpers/#{solution}"
            if File.exist?("#{@basePath}/blocks/#{solution}/#{action}")
                root = "#{@basePath}/blocks/#{solution}/#{action}"
            end
            envScript = "#{root}/environment.json"
            if File.exist?(envScript)
                @log.info("Adding environment variables...")
                envPush.addToEnv "#{envScript}"
            end

            if File.exist?("#{root}/#{action}.sh")
                type = "shell"
            elsif File.exist?("#{root}/#{action}.py")
                type = "python"
            end

            r = RunnerWorker.new @raw

            ENV['CATALOG_LOCATION'] = "#{@basePath}";

            ENV['ES_SOLUTION'] = solution;
            ENV['ES_ACTION'] = action;

            parameters = Template.new.processString(parameters, {})

            if (type == "ruby")
                cmd = "ruby #{root}/#{action}.rb '#{parameters}'"
            elsif (type == "python")
                cmd = "python #{root}/#{action}.py '#{parameters}'"
            else
                argList = []
                args = JSON.parse(parameters)
                args.each do | k |
                    argList.push('--' + k[0])
                    argList.push('"' + k[1] + '"')
                end

                cmd = "#{root}/#{action}.sh #{argList.join(' ')}"
            end
            r.run cmd, 1, 1, status

        rescue => exception
            @log.error(cmd)
            @log.error(exception.to_s)
            @log.error(exception.backtrace)
            abort()
        end
    end
end

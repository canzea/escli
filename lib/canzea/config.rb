require 'pathname'

module Canzea

  # Configuration defaults
  @config = {
              :log_level => "verbose",
              :config_location => "#{Dir.home}/.ecosystem-catalog",
              :catalog_location => "#{Dir.home}/.ecosystem-catalog/catalog",
              :catalog_git => "https://gitlab.com/canzea/ecosystem-catalog.git",
              :catalog_branch => "develop",
              :git_repo => "/opt/cloud-profile",
              :logging_root => Dir.home,
              :pwd => Dir.pwd,
              :consul_tls => false,
              :consul_tls_ca_file => "/etc/consul.d/ssl/ca.cert",
              :consul_tls_cert_file => "/etc/consul.d/ssl/consul.cert",
              :consul_tls_key_file => "/etc/consul.d/ssl/consul.key",
              :vault_tls_cert_file => "/etc/consul.d/ssl/vault.cert",
              :vault_tls_key_file => "/etc/consul.d/ssl/vault.key",
              :track_changes_in_git => false,
              :canzea_platform_uri => "https://canzea.com/console-app"
            }
  @valid_config_keys = @config.keys

  # Configure through hash
  def self.configure(opts = {})
    opts.each {|k,v| @config[k.to_sym] = v if @valid_config_keys.include? k.to_sym}
  end

  def self.config
    @config
  end

end

require "git"
require "fileutils"
require "pathname"
require "canzea/core/ssh-base-cmd-class"

class RemoteInit
    def initialize ()
        @log = Logger.new(Canzea::config[:logging_root] + '/plans.log')
    end

    def do (publicIp, privateKey)
        cmd1 = "yum -y install git"

        cmd2 = "(rm -rf ecosystem-catalog && git clone https://gitlab.com/canzea/ecosystem-catalog.git)"

        cmd3 = 'find /root/ecosystem-catalog -name "*.sh" -exec chmod +x {} +'

        cmd4 = '(cd /root/ecosystem-catalog && ./roles/workarounds/ruby/install.sh)'
        cmd5 = '(cd /root/ecosystem-catalog && ./roles/workarounds/ruby-gems/install.sh)'
        cmd6 = '(cd /root/ecosystem-catalog && ./roles/workarounds/sshd/configure.sh)'
        cmd7 = '(cd /root/ecosystem-catalog && ./roles/workarounds/sshd/enable.sh)'
        cmd8 = '(cd /root/ecosystem-catalog && ./roles/workarounds/root/install.sh)'
        cmd9 = '(cd /root/ecosystem-catalog && ./roles/workarounds/image-bootstrap/install.sh)'

        remote = RemoteCall.new

        remote.exec publicIp, privateKey, cmd1, "1-9"
        remote.exec publicIp, privateKey, cmd2, "2-9"
        remote.exec publicIp, privateKey, cmd3, "3-9"
        remote.exec publicIp, privateKey, cmd4, "4-9"
        remote.exec publicIp, privateKey, cmd5, "5-9"
        remote.exec publicIp, privateKey, cmd6, "6-9"
        remote.exec publicIp, privateKey, cmd7, "7-9"
        remote.exec publicIp, privateKey, cmd8, "8-9"
        remote.exec publicIp, privateKey, cmd9, "9-9"
    end

end

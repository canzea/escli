require 'json'
require 'logger'
require 'canzea/commands/add-env'
require 'canzea/plan-step-class'

class ApplyConfig
    def initialize ()
        @log = Logger.new(Canzea::config[:logging_root] + '/plans.log')
    end

    def do (gitRoot, test = false, stepNum = nil, task = nil)
        ps = PlanStep.new

        # Read the configuration file and make calls out to run

        log "Processing #{gitRoot}/configure.json"

        steps = JSON.parse(File.read("#{gitRoot}/configure.json"))


        index = 1
        steps["steps"].each { | step |

            # Needs to be after each step because new env vars could be set (i.e./ CONSUL_URL)
            AddEnv.new.injectEnvironmentVariables()

            ref = "Step #{index.to_s.rjust(2, "0")} / #{steps['steps'].length}"

            role = step['role']
            solution = step['solution']

            ENV['ES_STEP'] = "#{index}";

            if (stepNum == nil or index >= Integer(stepNum))
                log "  [#{ref}] Configure for #{role} and #{solution}"
                ps.runPhaseConfigure role, solution, test, (task == nil ? 1:task), ref
                task = 1
            else
                log "  [#{ref}] Configure for #{role} and #{solution} SKIP"
            end
            index = index + 1
        }
    end

    def log (msg)
        puts msg
        @log.info(msg)
    end

end
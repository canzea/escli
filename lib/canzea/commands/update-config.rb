require 'json'

class UpdateConfig

    def do (role, solution, gitRoot)

        steps = {}

        if File.directory? "#{gitRoot}"
            # write to configure the registration of the service
            if (File.exists?("#{gitRoot}/configure.json"))
                steps = JSON.parse(File.read("#{gitRoot}/configure.json"))
            else
                steps["steps"] = []
            end

            conf = {
                :role => role,
                :solution => solution
            }
            steps["steps"].push(conf)

            File.open("#{gitRoot}/configure.json", 'w') { |file| file.puts(JSON.generate(steps)) }
        end
    end
end

require "git"
require "fileutils"
require "pathname"

class ConfigGitCommit
    def initialize ()
        @log = Logger.new(Canzea::config[:logging_root] + '/plans.log')
    end

    def do(gitRoot, gitUri, sourcePath, template, basePath, parameters)
        begin
            log "Using basePath #{basePath}"

            if (File.exists?(gitRoot) == false)
                log "Cloning git repository #{gitUri}"
                Git.config do |config|
                  config.ssh_key = "#{Dir.home}/.ssh/id_rsa"
                end
                g = Git.clone(gitUri, "", :path => gitRoot)
            end

            # if file exists on file system and does not exist in gitRoot, then commit the "oem" file

            if (File.exists?(sourcePath))
                dest = "#{gitRoot}/#{basePath}#{Pathname.new(sourcePath).realpath}"
            else
                dest = "#{gitRoot}/#{basePath}#{sourcePath}"
            end
            log "Writing to #{dest}"

            if (File.exist?(dest) == false and File.exist?(sourcePath))
                FileUtils.mkdir_p Pathname.new(dest).dirname
                FileUtils.cp sourcePath, dest

                log "Recording in repo the default file first: #{sourcePath}"
                if (Canzea::config[:track_changes_in_git])
                    g = Git.init(gitRoot)
                    g.add(:all=>true)
                    g.commit("Original default #{sourcePath}")
                end
            end

            if template
                log "Processing template #{template}"
                FileUtils.mkdir_p Pathname.new(sourcePath).dirname
                t = Template.new
                t.processAndWriteToFile template, sourcePath, parameters
            end
            FileUtils.mkdir_p Pathname.new(dest).dirname
            FileUtils.cp sourcePath, dest, :verbose => true

            if (Canzea::config[:track_changes_in_git])
                g = Git.init(gitRoot)
                g.add(:all=>true)
                log "#{g.status.changed.size() + g.status.added.size()}"
                if (g.status.changed.size() > 0 or g.status.added.size() > 0)
                    g.commit("Config update #{sourcePath}")
                end
            end

            if (Canzea::config[:track_changes_in_git])
                g = Git.init(gitRoot)
                g.push
            end
        rescue => exception
            @log.error("ConfigGitCommit Failed")
            @log.error(exception.to_s)
            @log.error(exception.backtrace)
            abort()
        end
    end

    def log (msg)
        puts msg
        @log.info(msg)
    end

end

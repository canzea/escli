require "git"
require "fileutils"
require "logger"
require "pathname"


class GetCatalog
    def initialize ()
        @log = Logger.new(Canzea::config[:logging_root] + '/plans.log')
    end
    def do(tag = nil)
        catalogLocation = Canzea::config[:catalog_location]
        FileUtils.rm_rf "#{catalogLocation}"
        FileUtils.mkdir_p "#{catalogLocation}"
        catalogLocation = Pathname.new(catalogLocation).parent().realpath
        g = Git.clone(Canzea::config[:catalog_git], "catalog", :path => catalogLocation)
        if (tag != nil)
            puts "-- (RESET) For tag #{tag}"
            g.checkout(tag)
        else
            g.checkout(g.branch(Canzea::config[:catalog_branch]))
        end
        branch = g.current_branch
        puts "-- (RESET) Branch '#{branch}' Commit #{g.log[0]}"
        puts "-- (RESET) Source #{catalogLocation}/catalog"
    end

    def state()
        g = Git.open(Canzea::config[:catalog_location])

        branch = g.current_branch
        commit = g.log[0]
        g.tags.each do | c |
            # Doesn't seem to link tag to the branch so excluding for now
            # ENV.store('CATALOG_TAG', c.name)
            # puts "-- Tag #{c.name}"
        end

        ENV.store('CATALOG_BRANCH', branch)
        ENV.store('CATALOG_COMMIT', "#{g.log[0]}")

        # puts "-- Branch '#{ENV['CATALOG_BRANCH']}' Commit #{ENV['CATALOG_COMMIT']}"

    end

end

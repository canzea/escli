
=begin

curl -v -X GET -H "Content-Type: application/json" https://canzea.com/console-app/api/api/v2/saasexpress/ecosystem

Canzea::config[:canzea_platform_uri] + "/api/v2/saasexpress/ecosystem"

escli ecosystem resource test.yaml intg-0

escli resource plus test.yaml prod-1

escli resource apply f67c70b0-2706-48b5-ac4b-c76eeb72effe

=end

require 'net/http'
require 'json'
require 'canzea/commands/login'

class Resources
    def plus(manifest, segment)
        token = Login.new.get()

        esFile = Canzea::config[:config_location] + '/.scope'
        es = JSON.parse(File.read(esFile))

        puts "Using #{es['id']}"

        uri = URI(Canzea::config[:canzea_platform_uri] + "/api/v2/saasexpress/buildingblocks/#{es['id']}/buildingblocks/configure")

        config = File.read(manifest)

        req = Net::HTTP::Post.new(uri, 'Authorization' => "Bearer #{token}", 'Content-Type' => 'application/json')
        req.body = {config: config, segment: segment, resources: []}.to_json

        https = Net::HTTP.new(uri.hostname,uri.port)
        https.use_ssl = uri.instance_of? URI::HTTPS

        res = https.request(req)

        case res
            when Net::HTTPSuccess, Net::HTTPRedirection
                es = JSON.parse(res.body)
                puts JSON.pretty_generate(es)
                puts "Run 'escli resource apply #{es['id']}'"
            else
                puts "Error #{res}"
                puts res.body
        end

    end

    def apply(resourceID)
        token = Login.new.get()

        esFile = Canzea::config[:config_location] + '/.scope'
        es = JSON.parse(File.read(esFile))

        uri = URI(Canzea::config[:canzea_platform_uri] + "/api/v2/saasexpress/resources/plus/#{es['id']}/#{resourceID}")

        puts "Calling #{uri}"

        req = Net::HTTP::Post.new(uri, 'Authorization' => "Bearer #{token}", 'Content-Type' => 'application/json')
        req.body = {}.to_json

        https = Net::HTTP.new(uri.hostname,uri.port)
        https.use_ssl = uri.instance_of? URI::HTTPS

        res = https.request(req)

        case res
            when Net::HTTPSuccess, Net::HTTPRedirection
                es = JSON.parse(res.body)
                puts JSON.pretty_generate(es)
                puts "Successfully scheduled.  Batch reference # #{es['ref']}."
            else
                puts "Error #{res}"
                puts res.body
        end

    end
end

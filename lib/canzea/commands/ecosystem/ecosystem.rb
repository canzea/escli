
=begin

curl -v -X GET -H "Content-Type: application/json" https://canzea.com/console-app/api/api/v2/saasexpress/ecosystem

Canzea::config[:canzea_platform_uri] + "/api/v2/saasexpress/ecosystem"

escli ecosystem set 5b9c602fe4b02b1389591ecc

=end

require 'net/http'
require 'json'
require 'canzea/commands/login'

class Ecosystem
    def set(id)
        token = Login.new.get()

        uri = URI(Canzea::config[:canzea_platform_uri] + "/api/v2/saasexpress/ecosystem/#{id}")

        req = Net::HTTP::Get.new(uri, 'Authorization' => "Bearer #{token}")

        https = Net::HTTP.new(uri.hostname,uri.port)
        https.use_ssl = uri.instance_of? URI::HTTPS

        res = https.request(req)

        case res
            when Net::HTTPSuccess, Net::HTTPRedirection
                puts JSON.pretty_generate(JSON.parse(res.body))
                es = JSON.parse(res.body)

                scope = Canzea::config[:config_location] + '/.scope'
                File.open(scope, 'w') { |file| file.write({id:id}.to_json) }
                puts "Ecosystem set to #{es['ecosystemRefId']} with domain #{es['fqdomainName']}"
            else
                puts res.body
        end

    end

    def list()
        token = Login.new.get()

        uri = URI(Canzea::config[:canzea_platform_uri] + "/api/v2/saasexpress/ecosystem/summary")

        req = Net::HTTP::Get.new(uri, 'Authorization' => "Bearer #{token}")

        https = Net::HTTP.new(uri.hostname,uri.port)
        https.use_ssl = uri.instance_of? URI::HTTPS

        res = https.request(req)

        case res
            when Net::HTTPSuccess, Net::HTTPRedirection
                list = JSON.parse(res.body)
                list.each { | es |
                    printf "Ecosystem: %-25s %-15s %-15s %-8s %s\n", es['_id'], es['status'], es['fqdomainName'], es['ecosystemRefId'], es['name']
                }
            else
                puts res.body
        end

    end
end

require 'json'
require 'logger'
require 'pathname'
require 'canzea/helper-run-class'
require 'canzea/core/trace-component'
require 'canzea/core/prepare-environment'

class RegisterMetadata
    def initialize ()
        @basePath = "#{Pathname.new(Canzea::config[:catalog_location]).realpath}/"
        @log = Logger.new(Canzea::config[:logging_root] + '/plans.log')
    end

    def do (role, solution, test)

        plan = JSON.parse("{ \"plan\": [ { \"role\": \"#{role}\", \"solution\": \"#{solution}\" } ] }")

        n = Worker.new
        n.test ( test )

        lines = 1

        cmd = "undefined"

        begin

            plan['plan'].each do |item|

                root = "#{@basePath}/roles/#{item['role']}/#{item['solution']}"
                if File.exist?(root) == false
                    log "-- ERROR #{root} does not exist!"
                    raise "#{root} does not exist!"
                end

                # Register the service with Consul, if consul is ready
                # If metadata.json exists, then use the information to register
                cmd = "#{@basePath}/roles/#{item['role']}/#{item['solution']}/metadata.json"
                if File.exist?(cmd)
                    md = File.read(cmd)
                    md = JSON.parse(md)
                    if (md['services'].size() > 0)
                        svc = md['services'][0]

                        adef = {
                            "listener"=>svc['listener'],
                            "name" => "#{svc['name']}",
                            "id" => "#{ENV['HOSTNAME']}-#{svc['name']}",
                            "tags"=>[ item['role'] ],
                            "port"=>svc['port']
                        }

                        if (svc.has_key? "checks")
                            adef[:check] = svc['checks'][0]
                        end
                        log "-- Registering Service: #{svc['name']}"
                        log "--    #{adef.to_json}"
                        h = HelperRun.new
                        if test
                            log "--  TEST ONLY"
                        else
                            h.run "consul", "register_service", JSON.generate(adef)
                        end
                    end
                end
            end
        rescue => exception
            @log.error(cmd)
            @log.error(exception.to_s)
            @log.error(exception.backtrace)
            abort()
        end
    end

    def log (msg)
        puts msg
        @log.info(msg)
    end

end

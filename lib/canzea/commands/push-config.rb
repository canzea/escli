
require 'git'
require 'json'
require 'fileutils'
require 'logger'
require 'canzea/config'

# canzea --lifecycle=wire --solution=gogs --action=push-content --args='{"file":"metadata.json","path":"test/a/metadata.json", "comment":"V1.0"}'
# canzea --util=push-config git@165.227.87.135:root/ecosystem.git "Latest Input" --args='{"files":[{"file":"metadata.json","path":"test/a/metadata.json"}]}'

# PushConfig.new.write "Test", "nr-global.flow", "new content and more and more"

# PushConfig.new.cp "Another bit of file", "nr-global.flow", "node-red/nr-global.flow"


class PushConfig
    def initialize (_relPath = "instances/#{ENV['HOSTNAME']}/")
        @relPath = _relPath
        @log = Logger.new(Canzea::config[:logging_root] + '/plans.log')
    end

    def cat(filePath)
        write filePath, File.read(filePath)
    end

    def cp(filePath, destPath)
        if File.directory?(filePath)
            Dir.foreach(filePath) { |x|
                if x == '.' or x == '..' or x.start_with?('.')
                else
                    if File.directory?("#{filePath}/#{x}")
                        cp "#{filePath}/#{x}", "#{destPath}/#{x}"
                    else
                        write "#{destPath}/#{x}", File.read("#{filePath}/#{x}")
                    end
                end
            }
        else
            write destPath, File.read(filePath)
        end
    end

    def backupAndRemove(filePath)
        backup(filePath)
        rm(filePath)
    end

    def backup(filePath)
        path = "ecosystems/#{ENV['ECOSYSTEM']}/#{@relPath}#{filePath}"

        folder = "sc"
        if ENV.has_key? 'ECOSYSTEM_CONFIG_GIT'
          folder = "_working"
        end

        if File.exists? folder
            g = Git.init(folder)
        else
            if ENV.has_key? 'ECOSYSTEM_CONFIG_GIT'
                g = Git.clone(ENV['ECOSYSTEM_CONFIG_GIT'], folder, :path => '.')
            else
                g = Git.init(folder)
            end
        end

        puts "Exists? #{folder}/#{path}"
        if File.exists? "#{folder}/#{path}"
            numb = 1
            bkup = "#{folder}/#{path}.archived"
            while File.exists? "#{bkup}#{numb}"
                numb = numb + 1
            end
            puts "Backed up to #{bkup}#{numb}"
            File.write "#{bkup}#{numb}", File.read("#{folder}/#{path}");

            g.add("#{path}.archived#{numb}")
        end
    end

    def write(filePath, contents)

        path = "ecosystems/#{ENV['ECOSYSTEM']}/#{@relPath}#{filePath}"

        begin
            folder = "sc"
            if ENV.has_key? 'ECOSYSTEM_CONFIG_GIT'
              folder = "_working"
            end


            if File.exists? folder
                g = Git.init(folder)
            else
                if ENV.has_key? 'ECOSYSTEM_CONFIG_GIT'
                    g = Git.clone(ENV['ECOSYSTEM_CONFIG_GIT'], folder, :path => '.')
                else
                    g = Git.init(folder)
                end
            end
            FileUtils.mkdir_p File.dirname("#{folder}/#{path}")

            pth = File.expand_path "#{folder}/#{path}"

            @log.info "Writing to: #{pth}"

            open("#{folder}/#{path}", 'w') { |f|
              f.puts contents
            }

            g.add("#{path}")

        rescue => exception
            @log.error("PushConfig Failed")
            @log.error(exception.to_s)
            @log.error(exception.backtrace)
            abort()
        end

    end

    def rm(filePath)
        path = "ecosystems/#{ENV['ECOSYSTEM']}/#{@relPath}#{filePath}"

        begin
            folder = "sc"
            if ENV.has_key? 'ECOSYSTEM_CONFIG_GIT'
              folder = "_working"
            end

            if File.exists? folder
                g = Git.init(folder)
            else
                g = Git.clone(ENV['ECOSYSTEM_CONFIG_GIT'], folder, :path => '.')
            end

            if File.directory? "#{folder}/#{path}"
                puts "DELETE DIRECTORY: #{folder}/#{path}"
                FileUtils.rmdir_p File.dirname("#{folder}/#{path}")

                g.add("#{path}")

            else
                if File.exists? "#{folder}/#{path}"
                    puts "DELETE FILE: #{folder}/#{path}"
                    FileUtils.rm_f "#{folder}/#{path}"
                    g.remove("#{path}")
                end
            end
        rescue => exception
            @log.error("PushConfig Failed")
            @log.error(exception.to_s)
            @log.error(exception.backtrace)
            abort()
        end

    end

    def commit(comment)
        folder = "sc"
        if ENV.has_key? 'ECOSYSTEM_CONFIG_GIT'
          folder = "_working"
        else
          return
        end

        begin
            count = 0


            g = Git.init(folder)

            begin
                g.chdir do
                  g.status.changed.each do |file|
                        count = count +1
                  end
                  g.status.added.each do |file|
                        count = count +1
                  end
                  g.status.deleted.each do |file|
                        count = count +1
                  end
                end
            rescue => exception_ignore
                count = 1
            end

            if count == 0
              log "No changes"
            else
              log "#{count} changes committed."
              g.commit(comment)

              if ENV.has_key? 'ECOSYSTEM_CONFIG_GIT'
                  log "Pushing changes to SCM."
                  g.push
              end
            end

            begin
                if ENV.has_key? 'ECOSYSTEM_CONFIG_GIT'
                  log "Removing #{folder}"
                  FileUtils.rm_rf(folder)
                end
            rescue => exception
                @log.error("PushConfig Failed")
                @log.error(exception.backtrace)
                @log.error(exception.to_s)
            end

        rescue => exception
            @log.error("PushConfig Failed")
            @log.error(exception.backtrace)
            @log.error(exception.to_s)
            abort()
        end
    end

    def do(url, comment, params)
        begin

            # "git@#{ENV["GOGS_ADDRESS"]}:root/ecosystem.git"

            folder = "sc"
            if ENV.has_key? 'ECOSYSTEM_CONFIG_GIT'
              folder = "_working"
            end

            FileUtils.rm_rf(folder)

            g = Git.clone(url, folder, :path => '.')

            params['files'].each { | f |
                file = f['file']

                path = f['path']

                FileUtils.mkdir_p File.dirname("#{folder}/#{path}")

                open("#{folder}/#{path}", 'w') { |f|
                  f.puts File.read(file)
                }
                g.add("#{path}")
            }


            count = 0
            g.chdir do
              g.status.changed.each do |file|
                    count = count +1
              end
              g.status.added.each do |file|
                    count = count +1
              end
              g.status.deleted.each do |file|
                    count = count +1
              end
            end


            if count == 0
              log "No changes"
            else
              log "#{count} changes committed."
              g.commit(comment)

              log "Pushing changes to SCM."
              g.push
              log "Changes pushed"
            end
        rescue => exception
            @log.error("PushConfig Failed")
            @log.error(exception.to_s)
            @log.error(exception.backtrace)
            abort()
        end
    end

    def log (msg)
        puts msg
        @log.info(msg)
    end
end

require 'json'
require 'base64'
require 'yaml'
require 'canzea/config'
require 'canzea/plan-step-class'
require 'canzea/commands/remote-run'
require 'canzea/commands/add-env'

class PreparePlan
    def initialize ()
        @log = Logger.new(Canzea::config[:logging_root] + '/plans.log')
        @basePath = "#{Pathname.new(Canzea::config[:catalog_location]).realpath}"
    end

    def doPatch (paramsString, test, privateKey, serverBase, serverNumber)

        publicIp = File.read("#{Canzea::config[:pwd]}/vps-#{serverBase}-#{serverNumber}.json")
        publicIp.strip!

        params = JSON.parse(paramsString)

        patches = Base64.decode64(params["patches"])
        params['patches'] = JSON.parse(patches)

        params['patches'].each do | patch |
            RemoteRun.new.doCommand publicIp, privateKey, patch["command"]
        end
    end

    # Read the blueprint instructions and prepare plan for a particular segment
    def do (blueprint, segment, step, task, test, privateKey, serverBase, serverNumber)
        planStep = PlanStep.new

        log "Processing configure.json for #{segment} in #{blueprint} from #{@basePath}"

        instructions = YAML.load_file("#{@basePath}/blueprints/#{blueprint}/instruction.yml")
        segment = instructions['instructions']['segments'][segment]

        log segment['abbreviation']

        first = true
        index = 1
        segment['image'].each { |item|
            if item.start_with?("step:")
                parts = item.split(':')

                if (index < Integer(step))
                    log "[#{index.to_s.rjust(2, "0")}] #{item} SKIPPING"
                    msg = { "message" => { "task" => "skip", "context" => { "step" => index } } }
                    puts msg.to_json
                else

                    log "[#{index.to_s.rjust(2, "0")}] #{item}"
                    publicIp = File.read("#{Canzea::config[:pwd]}/vps-#{serverBase}-#{serverNumber}.json")
                    publicIp.strip!
                    if (test == false)
                        if (task == nil)
                            RemoteRun.new.do publicIp, privateKey, parts[1], parts[2], index.to_s.rjust(2, "0")
                        else
                            RemoteRun.new.doTask publicIp, privateKey, parts[1], parts[2], task, index.to_s.rjust(2, "0")
                        end
                        # Keep track of what we have done; parsing the response and looking at the JSON
                    else
                        RemoteRun.new.test publicIp, privateKey, parts[1], parts[2], index.to_s.rjust(2, "0")
                    end

                    if first == true
                        # task is only relevant for the first invocation
                        task = nil
                        first = false
                    end
                end
                index = index + 1
            end
        }
    end

    def log (msg)
        puts "-- #{msg}"
        @log.info(msg)
    end
end
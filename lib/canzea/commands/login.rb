
=begin
Login should use an ecosystem.

curl -v -X POST -H "Content-Type: application/json" -d '{"name":"","password":""}' https://canzea.com/console-app/api/x/user/login

Canzea::config[:canzea_platform_uri] + "/api/x/user/login"
=end

require 'net/http'
require 'json'

class Login
    def do(name, password)
        credFile = "#{Dir.home}/.canzearc"

        uri = URI(Canzea::config[:canzea_platform_uri] + "/api/x/user/login")

        req = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
        req.body = {name: name, password: password}.to_json

        https = Net::HTTP.new(uri.hostname,uri.port)
        https.use_ssl = uri.instance_of? URI::HTTPS

        res = https.request(req)

        case res
            when Net::HTTPSuccess, Net::HTTPRedirection
                token = JSON.parse(res.body)

                File.open(credFile, 'w') { |file| file.write(token['token']) }
                puts "Login successful.  Authorization cached."
            else
              puts res.body
        end

    end

    def logout()
        credFile = "#{Dir.home}/.canzearc"
        File.delete(credFile)
        puts "Authorization cache cleared."
    end

    def get()
        credFile = "#{Dir.home}/.canzearc"
        token = File.read(credFile)
        return token
    end
end

require "git"
require "fileutils"
require "pathname"
require "canzea/helper-run-class"

class GenUser
    def do(name)
        puts "Generating #{name}"

        home = "/home/#{name}"
        key = "general"

        plans = [
            {:solution => "users", :action => "add-user", :parameters => { "name" => name }},
            {:solution => "users", :action => "new-key", :parameters => { "name" => name, "home" => home, "key" => key }},
        ]

        helper = HelperRun.new(false)
        helper.runPlan(plans)

    end
end

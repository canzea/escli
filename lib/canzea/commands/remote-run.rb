require "json"
require "git"
require "fileutils"
require "pathname"
require "canzea/core/ssh-base-cmd-class"

class RemoteRun
    def doWire(publicIp, privateKey, solution, action, args, ref="")
      remote = RemoteCall.new
      remote.exec publicIp, privateKey, "canzea --lifecycle=wire --raw --solution=#{solution} --action=#{action} --args='#{args}'", ref
    end

    def doTask(publicIp, privateKey, role, solution, task, ref="")
      remote = RemoteCall.new
      remote.exec publicIp, privateKey, "canzea --lifecycle=install --role=#{role} --task=#{task} --solution=#{solution}", ref
    end

    def do(publicIp, privateKey, role, solution, ref="")
      remote = RemoteCall.new
      remote.exec publicIp, privateKey, "canzea --lifecycle=install --role=#{role} --solution=#{solution}", ref
    end

    def doConfigure(publicIp, privateKey, role, solution, ref="")
      remote = RemoteCall.new
      remote.exec publicIp, privateKey, "canzea --lifecycle=configure --role=#{role} --solution=#{solution}", ref
    end

    def doCommand(publicIp, privateKey, command, ref="")
      remote = RemoteCall.new
      remote.exec publicIp, privateKey, command, ref
    end

    def test(publicIp, privateKey, role, solution, ref="")
      remote = RemoteCall.new
      remote.exec publicIp, privateKey, "canzea --lifecycle=install --role=#{role} --solution=#{solution} --test", ref
    end
end

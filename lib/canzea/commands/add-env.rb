require 'json'
require 'logger'
require 'canzea/plan-step-class'

class AddEnv
    def initialize (_raw = false)
        @log = Logger.new(Canzea::config[:logging_root] + '/plans.log')
        @raw = _raw
    end

    def add (key, value)
        extraConfig = Canzea::config[:config_location] + "/env.json"
        envs = loadFile()
        envs["vars"][key] = value
        File.write(extraConfig, JSON.generate(envs))
    end

    def addSecret (key, value)
        extraConfig = Canzea::config[:config_location] + "/env.json"
        envs = loadFile()
        envs['secrets'][key] = value
        File.write(extraConfig, JSON.generate(envs))
    end

    def loadFile()
        extraConfig = Canzea::config[:config_location] + "/env.json"
        if File.exists?(extraConfig)
            file = File.read(extraConfig)
            envs = JSON.parse(file)
        else
            envs = {"vars"=>{}, "secrets"=>{}}
        end
        return envs
    end

    def injectEnvironmentVariables()
        extraConfig = Canzea::config[:config_location] + "/env.json"
        @log.info "Looking at for env vars: #{extraConfig}"
        if File.exists?(extraConfig)
            pputs "-- Reading #{extraConfig}"
            file = File.read(extraConfig)
            envs = JSON.parse(file)
            envs['vars'].keys.each { | key |
                val = envs['vars'][key]
                pputs "--    #{key} == #{val}"
                @log.info "Setting: #{key} == #{val}"
                ENV.store(key, val)
            }
            envs['secrets'].keys.each { | key |
                val = envs['secrets'][key]
                pputs "--    #{key} == XXXXXX"
                @log.info "Setting: #{key} == XXXXXXX"
                ENV.store(key, val)
            }
        end
    end

    def pputs (s)
        if (@raw == false)
            puts "#{s}"
        end
    end

end
require 'json'
require_relative 'config'
require_relative 'canzea/core/registry'

module Canzea

    extraConfig = Canzea::config[:config_location] + "/config.json"
    if File.exists?(extraConfig)
        file = File.read(extraConfig)
        Canzea::configure JSON.parse(file)
    end
end
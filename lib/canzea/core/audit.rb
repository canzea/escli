require 'json'
require 'open3'
require 'stringio'
require 'logger'


class Audit
    def initialize (_raw)
        @raw = _raw;
    end

    def start (id, cmd)
        self.log( id, cmd, "start", "", 0, "")
    end

    def complete (id, cmd, status, msecs, result)
	    self.log( id, cmd, "complete", status, msecs, result)
    end

    def status (id, cmd, status, msecs, result)
        data = {
            "message" => {
                "id" => id,
                "cmd" => cmd,
                "task" => "status",
                "status" => status,
                "elapsed" => msecs,
                "result" => result
            }
        }
        context = {
            "step" => ENV['ES_STEP'],
            "ref" => ENV['ES_REF'],
            "role" => ENV['ES_ROLE'],
            "solution" => ENV['ES_SOLUTION'],
            "action" => ENV['ES_ACTION']
        }
        data['message'][:context] = context

        pputs data.to_json
        File.open(Canzea::config[:logging_root] + '/audit.log', 'a') { |file| file.puts(data.to_json) }
    end

    def log (id, cmd, task, status, msecs, result)

        data = {
            "message" => {
                "id" => id,
                "cmd" => cmd,
                "task" => task,
                "status" => status,
                "elapsed" => msecs,
                "result" => result
            }
        }
        summary = {
            "message" => {
                "id" => id,
                "cmd" => cmd,
                "task" => task,
                "status" => status,
                "elapsed" => msecs
            }
        }
        context = {
            "step" => ENV['ES_STEP'],
            "ref" => ENV['ES_REF'],
            "role" => ENV['ES_ROLE'],
            "solution" => ENV['ES_SOLUTION'],
            "action" => ENV['ES_ACTION']
        }
        data['message'][:context] = context
        summary['message'][:context] = context

        pputs summary.to_json

        File.open(Canzea::config[:logging_root] + '/audit.log', 'a') { |file| file.puts(data.to_json) }
    end

    def pputs (s)
        if (@raw == false)
            puts "#{s}"
        end
    end

end
require 'json'
require 'open3'
require 'stringio'
require 'logger'
require 'canzea/core/audit'
require 'canzea/config'

def time_diff_milli(start, finish)
   (finish - start) * 1000.0
end

def handleIO(stillOpen, ioArray, io, log)
  if ioArray.include?(io)
    begin
      log.write(io.readpartial(4096))
    rescue EOFError
      stillOpen.delete_if{|s| s == io}
    end
  end
end

class RunnerWorker
    def initialize (_raw)
        @raw = _raw;
    end

    def run(command, start, lines, statusIndicator = false)
        audit = Audit.new @raw

	    log = Logger.new(Canzea::config[:logging_root] + '/plans.log')

        log.info("HANDLING: " + command)

        l = command

        audit.start( "#{lines + 1 }", l.chomp)

        t1 = Time.now

        workingDir = Canzea::config[:pwd]

        pputs "Executing [#{workingDir}] #{l}"
        Dir.chdir(workingDir){

            Open3.popen3(ENV, l) {|stdin, stdout, stderr, wait_thr|
              pid = wait_thr.pid # pid of the started process.

              log_w = StringIO.new

              stillOpen = [stdout, stderr]
              while !stillOpen.empty?
                fhs = select(stillOpen, nil, nil, nil)
                handleIO(stillOpen, fhs[0], stdout, log_w)
                handleIO(stillOpen, fhs[0], stderr, log_w)
              end

              exit_status = wait_thr.value # wait for it to finish

              log_w.close_write

              t2 = Time.now
              msecs = time_diff_milli t1, t2

              output = log_w.string

              output.scan(/.{1,80}/).each do | line |
                  log.info("STDOUT: #{line}")
              end

              log.info("Exit Status = #{exit_status}")
              log.info("Completed in (ms) : #{msecs}")

              pputs "Exit Status = #{exit_status}"
              output.split(/\n/).each do | line |
                  pputs "#{line}"
              end


              # if exit status is failure then exit right away

              audit.complete("#{lines + 1}", l.chomp, exit_status.exitstatus, msecs, output)

              if (statusIndicator)
                  audit.status("#{lines + 1}", l.chomp, exit_status.exitstatus, msecs, output)
              end

              if (@raw)
                  puts output
              end

              if exit_status.exitstatus != 0
                abort()
              end
            }
        }

        lines += 1

        return lines

    end

    def pputs (s)
        if (@raw == false)
            puts "-- #{s}"
        end
    end
end
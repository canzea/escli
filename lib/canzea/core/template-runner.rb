require 'json'
require 'mustache'


module ViewHelpers
  # {{#tf_name}}name{{/tf_name}}
  def tf_name(name)
    self[name].sub '.', '-'
  end
end

class Template < Mustache
  include ViewHelpers

  def processAndWriteToFile (template, output, parameters)
     contents = process template, parameters
     File.write(output, contents)
  end

  def process (template, parameters)
     self.template_file = template
     ENV.each_pair do |k, v|
       self[k] = v
     end
     parameters.each_pair do |k, v|
       self[k] = v
     end
     return self.render
  end

  def processString (string, parameters)
     self.template = string
     ENV.each_pair do |k, v|
       self[k] = v
     end
     parameters.each_pair do |k, v|
       self[k] = v
     end
     return self.render
  end
end

require 'json'
require 'logger'
require 'pathname'
require 'canzea/helper-run-class'
require 'canzea/core/prepare-environment'
require 'canzea/core/trace-component'
require 'canzea/commands/push-config'

class PlanStep
    def initialize ()
        @basePath = "#{Pathname.new(Canzea::config[:catalog_location]).realpath}"
        @log = Logger.new(Canzea::config[:logging_root] + '/plans.log')
    end

    def runPhaseInstall (role, solution, test, task)

        plan = JSON.parse("{ \"plan\": [ { \"role\": \"#{role}\", \"solution\": \"#{solution}\" } ] }")

        ENV['ES_ROLE'] = role;
        ENV['ES_SOLUTION'] = solution;

        n = Worker.new
        n.test ( test )

        start = Integer(task)
        lines = 1

        cmd = "undefined"

        begin

            plan['plan'].each do |item|
                @log.info(item['solution'])

                root = "#{@basePath}/roles/#{item['role']}/#{item['solution']}"
                if File.exist?(root) == false
                    root = "#{@basePath}/blocks/#{item['solution']}"
                    if File.exist?(root) == false
                        puts "-- ERROR #{root} does not exist!"
                        raise "#{root} does not exist!"
                    end
                end

                cmd = "#{root}/install.sh"
                if File.exist?(cmd)
                    lines = n.run cmd, start, lines - 1

                    @log.info "#{ lines } lines read"
                end
            end
        rescue => exception
            @log.error(cmd)
            @log.error(exception.to_s)
            @log.error(exception.backtrace)
            abort()
        end
    end

    def runPhaseConfigure (role, solution, test, task, ref="")

        plan = JSON.parse("{ \"plan\": [ { \"role\": \"#{role}\", \"solution\": \"#{solution}\" } ] }")

        ENV['ES_REF'] = ref;
        ENV['ES_ROLE'] = role;
        ENV['ES_SOLUTION'] = solution;

        n = Worker.new
        n.test ( test )

        start = Integer(task)
        lines = 1

        cmd = "undefined"

        begin

            plan['plan'].each do |item|

                root = "#{@basePath}/roles/#{item['role']}/#{item['solution']}"
                if File.exist?(root) == false
                    root = "#{@basePath}/blocks/#{item['solution']}"
                    if File.exist?(root) == false
                        log "-- ERROR #{root} does not exist!"
                        raise "#{root} does not exist!"
                    end
                end

                if (test == false)
                    # Register the service with Consul, if consul is ready
                    # If metadata.json exists, then use the information to register
                    cmd = "#{root}/metadata.json"
                    if File.exist?(cmd)
                        md = File.read(cmd)
                        md = JSON.parse(md)
                        if (md['services'].size() > 0)
                            svc = md['services'][0]

                            adef = {
                                "listener"=>svc['listener'],
                                "name" => "#{svc['name']}",
                                "id" => "#{ENV['HOSTNAME']}-#{svc['name']}",
                                "tags"=>[ item['role'] ],
                                "port"=>svc['port']
                            }
                            log "-- Registering #{svc['name']}"
                            h = HelperRun.new false
                            h.run "consul", "register_service", JSON.generate(adef)
                        end
                    end

                    envScript = "#{root}/environment.json"
                    if File.exist?(envScript)
                        envPush = PrepareEnvironment.new false
                        envPush.addToEnv "#{envScript}"
                    end

                end

                cmd = "#{root}/configure.sh"
                if File.exist?(cmd)
                  lines = n.run cmd, start, lines - 1, false, ref
                end

                cmd = "#{root}/enable.sh"
                if File.exist?(cmd)
                  lines = n.run cmd, start, lines - 1, false, ref
                end

                cmd = "#{root}/status.sh"
                if File.exist?(cmd)
                  lines = n.run cmd, start, lines - 1, true, ref
                end

                if ENV.has_key? "ECOSYSTEM_CONFIG_GIT"
                    log "-- Writing data to ecosystem git"
                    adef = {
                        "ES_ROLE" => role,
                        "ES_SOLUTION" => solution
                    }
                    h = HelperRun.new false
                    h.run "ecosystem", "analyze-solution", JSON.generate(adef)
                end
            end
        rescue => exception
            @log.error(cmd)
            @log.error(exception.to_s)
            @log.error(exception.backtrace)
            abort()
        end
    end


    def runPhasePatch (commit, solution, test, task, ref="")

        plan = JSON.parse("{ \"plan\": [ { \"commit\": \"#{commit}\", \"solution\": \"#{solution}\" } ] }")

        ENV['ES_REF'] = ref;
        ENV['ES_ROLE'] = 'patch';
        ENV['ES_SOLUTION'] = solution;

        n = Worker.new
        n.test ( test )

        start = Integer(task)
        lines = 1

        cmd = "undefined"

        # patches/<commit>/<solution>
        begin

            plan['plan'].each do |item|

                root = "#{@basePath}/patches/#{item['commit']}/#{item['solution']}"
                if File.exist?(root) == false
                    log "-- WARNING #{root} does not exist"
                else

                    if (test == false)

                        envScript = "#{root}/environment.json"
                        if File.exist?(envScript)
                            envPush = PrepareEnvironment.new false
                            envPush.addToEnv "#{envScript}"
                        end

                    end

                    ENV.store('SCRIPT_PATH', root)

                    cmd = "#{root}/patch.sh"
                    if File.exist?(cmd)
                      lines = n.run cmd, start, lines - 1, false, ref
                    end
                end
            end
        rescue => exception
            @log.error(cmd)
            @log.error(exception.to_s)
            @log.error(exception.backtrace)
            abort()
        end
    end

    def log (msg)
        puts msg
        @log.info(msg)
    end

end
